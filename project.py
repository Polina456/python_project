import requests
from telegram.ext import Application, CommandHandler, MessageHandler, filters
from telegram import ReplyKeyboardMarkup, InputMediaPhoto
from bs4 import BeautifulSoup

TOKEN = '5899697947:AAGDWjWOvUcWpqObkaAHofoyMzAt4TJaLEg'

command_1 = "Известные студии"
command_2 = "Аниме за 2022"
command_3 = "Топ 10"
command_4 = "Аниме-анонсы"

anime_markup = [[command_1, command_2],
                [command_3, command_4]]

markup = ReplyKeyboardMarkup(anime_markup, one_time_keyboard=False, resize_keyboard=True)


async def start_hundler(update, context):
    await update.message.reply_text(
        "Привет!\n"
        "Я бот-оценщик аниме. Что бы вы хотели узнать?\n", reply_markup=markup)


# about в titles нужен title
async def info_hundler(update, context):
    response = requests.get('https://api.jikan.moe/v4/producers?limit=10').json()
    for data in response['data'][:5]:
        about = data['about']
        title = next((t for t in data['titles'] if t['type'] == 'Default'), None)
        if about and title:
            await update.message.reply_text(
                '\n{}, {}'.format(title['title'], about))


async def anime2022_hundler(update, context):
    text = []
    response = requests.get('https://api.jikan.moe/v4/seasons/2022/fall?limit=10').json()
    for data in response['data']:
        if data['score'] and data['episodes'] and (
                title := next((t for t in data['titles'] if t['type'] == 'Default'), None)):
            score, episodes = data['score'], data['episodes']
            text.append(
                'Название: {},\n Кол-во серий: {},\n Оценка пользвателей: {}\n'.format(title['title'], episodes, score))
            if data['trailer']['url']:
                await update.message.reply_text(
                    'Название: {},\n Трейлер: {}\n'.format(title['title'], data['trailer']['url']))
    await update.message.reply_text(
        'Cписок аниме за 2022:\n {}\n'.format(
            '\n'.join(text)))


async def top_anime_hundler(update, context):
    text = []
    media = []
    response = requests.get('https://api.jikan.moe/v4/top/anime?page=1&limit=10').json()
    for data in response['data']:
        title = next(title for title in data['titles'] if title['type'] == 'Default')
        score, episodes, image = data['score'], data['episodes'], data['images']["jpg"]['large_image_url']
        text.append(
            'Название: {},\n Кол-во серий: {},\n Оценка пользвателей: {}\n'.format(title['title'],
                                                                                   episodes, score))
        media.append(InputMediaPhoto(image))

    await update.message.reply_text(
        'TOP-10 аниме:\n {}\n'.format('\n'.join(text)))
    if media:
        await update.message.reply_media_group(media)


async def new_anime_hundler(update, context):
    text = []
    media = []
    url = 'https://myanimelist.net/anime/season/2023/summer?limit=5'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    anime_list = soup.find_all('div', class_='seasonal-anime')
    for anime in anime_list[:10]:
        name = anime.find('a', class_='link-title').text
        date = anime.find('span', class_="item").text
        genres = ', '.join(genre.text for genre in anime.find_all('span', class_='genre')).replace('\n', '')
        image = anime.find('img')['src']
        text.append('Название: {},\n Дата выхода: {},\n Жанры: {}\n'.format(name,
                                                                            date, genres))
        media.append(InputMediaPhoto(image))
    await update.message.reply_text(
        'Новые аниме этим летом:\n {}\n'.format('\n'.join(text)))
    if media:
        await update.message.reply_media_group(media)


async def ans(update, context):
    user_id = update.message.from_user.id
    with open(f'{user_id}.log', 'a') as log_file:
        log_file.write(f'{update.message.text}\n')

    if update.message.text == command_1:
        await info_hundler(update, context)
    elif update.message.text == command_2:
        await anime2022_hundler(update, context)
    elif update.message.text == command_3:
        await top_anime_hundler(update, context)
    elif update.message.text == command_4:
        await new_anime_hundler(update, context)


application = Application.builder().token(TOKEN).build()

application.add_handler(CommandHandler('start', start_hundler))
application.add_handler(MessageHandler(filters.TEXT, ans))

application.run_polling()
